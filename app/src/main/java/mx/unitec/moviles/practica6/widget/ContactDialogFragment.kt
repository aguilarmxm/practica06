package mx.unitec.moviles.practica6.widget

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import mx.unitec.moviles.practica6.R
import mx.unitec.moviles.practica6.model.Contact
import java.lang.ClassCastException

class ContactDialogFragment : DialogFragment() {

    internal lateinit var listener: NoticeDialogListener

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val view = inflater.inflate(R.layout.dialog_contact, null)

            builder.setView(view)
                .setTitle(R.string.dialog_title)
                .setPositiveButton(R.string.dialog_add,
                    DialogInterface.OnClickListener { dialogInterface, i ->
                        val mName : EditText = view.findViewById(R.id.ettName)
                        val mPhone : EditText = view.findViewById(R.id.ettPhone)
                        val mEmail : EditText = view.findViewById(R.id.ettEmail)
                        //agregar validaciones
                        val contact = Contact(mName.text.toString(),
                            mPhone.text.toString(),
                            mEmail.text.toString())
                        listener.onDialogPositiveClick(this, contact)
                       // Toast.makeText(activity, "Agregado", Toast.LENGTH_LONG).show()
                    })
                .setNegativeButton(R.string.dialog_cancel,
                DialogInterface.OnClickListener { dialogInterface, i ->
                    listener.onDialogNegativeClick(this)
                    dialog!!.cancel()
                })
            builder.create()
        } ?: throw IllegalStateException("La actividad no puede ser vacía")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            listener = context as NoticeDialogListener
        } catch (e: ClassCastException) {
            throw  ClassCastException(context.toString() +
                    " debe implementar NoticeDialogListener")
        }
    }
}